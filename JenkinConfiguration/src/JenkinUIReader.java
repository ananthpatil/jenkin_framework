import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class JenkinUIReader
 */
@SuppressWarnings("serial")
public class JenkinUIReader extends HttpServlet {
	// private static final long serialVersionUID = 1L;

	private int threadCount = 1;

	// private static List<Long> userId = new ArrayList<Long>();
	// private Long currentThread;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected synchronized void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter writer = response.getWriter();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// synchronized (writer) {
		String yamlTemplate, scriptPath, jmeterPath;
		try {
			// threadCount++;
			List<GetterAndSetter> getterSetterList = new ArrayList<GetterAndSetter>();

			// servlet context section
			ServletContext context = getServletContext();
			String jenkinUserName = context.getInitParameter("jenkinUserName");
			String jenkinPassword = context.getInitParameter("jenkinPassword");
			String jenkinHost = context.getInitParameter("jenkinHost");

			String newYamlFilePath = context
					.getInitParameter("jenkinWorkSpacePath");
			// String scriptPath = context.getInitParameter("ScriptPath");
			String jmeterFrameworkJobName = context
					.getInitParameter("jmeterFrameworkJobName");
			String scriptJobName = context.getInitParameter("scriptJobName");
			String automationJobName = context
					.getInitParameter("automationJobName");
			String reportJobName = context.getInitParameter("reportJobName");
			String pipelineJobName = context
					.getInitParameter("pipelineJobName");

			String jmeterVersion = context.getInitParameter("jmeterVersion");

			String slaveWorkSpacePath = context
					.getInitParameter("slaveWorkSpacePath");
			String mode = request.getParameter("mode");

			scriptPath = newYamlFilePath + File.separator + "workspace"
					+ File.separator + scriptJobName + File.separator;
			scriptPath = scriptPath.replace("\\", "/");
			jmeterPath = newYamlFilePath + File.separator + "workspace"
					+ File.separator + jmeterFrameworkJobName + File.separator
					+ "apache-jmeter-" + jmeterVersion + File.separator;
			jmeterPath = jmeterPath.replace("\\", "/");

			// getter setter section
			GetterAndSetter setterObj = new GetterAndSetter();
			setterObj.setExecutionMode(mode);
			// setterObj.setNodeLabelName(nodeName);

			setterObj.setJmeterGitPath(request.getParameter("jmeter"));
			setterObj.setJmeterGitBranch(request.getParameter("jbranch"));

			setterObj.setScriptGitPath(request.getParameter("script"));
			setterObj.setScriptGitBranch(request.getParameter("sbranch"));

			setterObj.setReptCodeGitPath(request.getParameter("report"));
			setterObj.setReptCodeGitBranch(request.getParameter("rbranch"));

			// setterObj.setJenkinWorkSpacePath(request.getParameter("yamlFilePath"));
			setterObj.setMemorySize(request.getParameter("memory"));

			setterObj.setScheduler(request.getParameter("scheduler"));
			setterObj.setYamlFileName(request.getParameter("yaml"));

			setterObj.setNode1IP(request.getParameter("node1"));
			setterObj.setScript1Name(request.getParameter("script1"));

			setterObj.setNode2IP(request.getParameter("node2"));
			setterObj.setScript2Name(request.getParameter("script2"));

			setterObj.setNode3IP(request.getParameter("node3"));
			setterObj.setScript3Name(request.getParameter("script3"));

			setterObj.setNode4IP(request.getParameter("node4"));
			setterObj.setScript4Name(request.getParameter("script4"));

			setterObj.setNode5IP(request.getParameter("node5"));
			setterObj.setScript5Name(request.getParameter("script5"));

			getterSetterList.add(setterObj);

			// section for updating the configuration file of all jobs
			// configObj.intializeList(getterSetterList, scriptPath);

			JobConfigurationReading configObj = new JobConfigurationReading(
					getterSetterList, scriptPath, jmeterPath,
					slaveWorkSpacePath, jenkinHost);

			if (mode.equalsIgnoreCase("distributed"))
				yamlTemplate = context.getInitParameter("distTemplate");
			else
				yamlTemplate = context.getInitParameter("standTemplate");

			IPMapping_BuildJmeterAndScriptJobs ipObject = new IPMapping_BuildJmeterAndScriptJobs(
					jenkinUserName, jenkinPassword, jenkinHost, setterObj,
					configObj, context, getterSetterList, mode);
			ipObject.nodeIpMapping();
			String multiJobStatus = ipObject.getJobStatus();
			int scriptIndex = ipObject.getScriptIndex();
	
			if (multiJobStatus.trim().toLowerCase().equals("success")) {
				String filePath = configObj.readConfigTemplate(
						context.getRealPath("/"), yamlTemplate,
						newYamlFilePath, scriptIndex);
				if (filePath != null) {
					filePath = scriptPath + filePath;
					CreateResultFolder resultObj = new CreateResultFolder(
							filePath);
					String resultFolderPath = resultObj.createZipFolder();

					configObj.readExistingJmeterAutomationJobConfig(
							jenkinUserName, jenkinPassword, jenkinHost,
							newYamlFilePath, resultFolderPath,
							automationJobName);

					configObj.readExistingReportJobConfig(jenkinUserName,
							jenkinPassword, jenkinHost, resultFolderPath,
							reportJobName);

					// section to build the job
					BuildJob pipelineObj = new BuildJob(pipelineJobName);
					String previousBuildId = pipelineObj.retrieveBuilID(
							jenkinUserName, jenkinPassword, jenkinHost);

					pipelineObj.execeuteJob(jenkinUserName, jenkinPassword,
							jenkinHost);

					String buildStatus = pipelineObj.retrieveBuildStatus(
							jenkinUserName, jenkinPassword, jenkinHost,
							previousBuildId);

					List<String> consoleLog = pipelineObj.retrieveBuildConsole(
							jenkinUserName, jenkinPassword, jenkinHost);

					request.setAttribute("consoleOutput", consoleLog);
					request.setAttribute("buildStatus", buildStatus);
					RequestDispatcher rd = request
							.getRequestDispatcher("ServerResponse");
					rd.forward(request, response);
				} else {
					RequestDispatcher rd = request
							.getRequestDispatcher("serverresponse_failed1.html");
					rd.forward(request, response);
				}
			} else {
				RequestDispatcher rd = request
						.getRequestDispatcher("serverresponse_failed2.html");
				rd.forward(request, response);
			}
		} catch (Exception e) {
			request.setAttribute("exception", e.toString());
			RequestDispatcher rd = request
					.getRequestDispatcher("serverresponse2.jsp");
			rd.forward(request, response);
		}
		writer.close();
		// }
	}
}
