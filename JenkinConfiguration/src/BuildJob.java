import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.DatatypeConverter;


public class BuildJob {

	private String jobName;

	BuildJob(String jobName) {
		this.jobName = jobName;
	}

	/**
	 * <p>
	 * The method used to build the job
	 * </p>
	 * 
	 * @param userName
	 * @param password
	 * @param hostWithPort
	 * @return void
	 * @throws Exception
	 */
	public void execeuteJob(String userName, String password,
			String hostWithPort) throws Exception {
		try {
			URL url = new URL("http://" + hostWithPort + "/job/" + jobName
					+ "/build");
			String authStr = userName + ":" + password;
			String encoding = DatatypeConverter.printBase64Binary(authStr
					.getBytes("utf-8"));
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);
			connection.setRequestProperty("Authorization", "Basic " + encoding);
			InputStream content = connection.getInputStream();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					content));
			String line;
			while ((line = in.readLine()) != null) {
				System.out.println(line);
			}
		} catch (Exception ex) {
			System.out.println("Exception in execeuteJob method:" + ex);
			throw ex;
		}
	}

	/**
	 * <p>
	 * The method is used to fetch the build ID of current build from jenkin
	 * </p>
	 * 
	 * @param userName
	 * @param password
	 * @param hostWithPort
	 * @return String
	 * @throws Exception
	 */
	public String retrieveBuilID(String userName, String password,
			String hostWithPort) {
		String buildID = null;
		try {
				URL url = new URL("http://" + hostWithPort + "/job/" + jobName
						+ "/api/json");
				String authStr = userName + ":" + password;
				String encoding = DatatypeConverter.printBase64Binary(authStr
						.getBytes("utf-8"));

				HttpURLConnection connection = (HttpURLConnection) url
						.openConnection();
				connection.setRequestMethod("POST");
				connection.setDoOutput(true);
				connection.setRequestProperty("Authorization", "Basic "
						+ encoding);
				InputStream content = connection.getInputStream();
				BufferedReader in = new BufferedReader(new InputStreamReader(
						content));
				String line, line1 = null;
				while ((line = in.readLine()) != null) {
					line1 = line;
				}
				/*if ((line1.contains(blueStatus) || line1.contains(redStatus))
						|| line1.contains(abortStatus)) {*/
				Pattern pat = Pattern
						.compile("\"lastBuild\".+?\"number\":(.+?),");
				Matcher mat = pat.matcher(line1);
				while (mat.find()) {
					buildID = mat.group(1).trim();
				}
		} catch (Exception e) {
			System.out.println("Exception in execeuteJob method:" + e);
		}
		return buildID;
	}

	/**
	 * <p>
	 * The method is used to fetch the build status information
	 * </p>
	 * 
	 * @param userName
	 * @param password
	 * @param hostWithPort
	 * @return void
	 * @throws Exception
	 */
	public String retrieveBuildStatus(String userName, String password,
			String hostWithPort,String previousBuildId) throws Exception {
		/*String buildId = retrieveBuilID(userName, password, hostWithPort,
				"blue_anime", "red_anime", "aborted_anime");*/
		boolean flag1 = true;
		String buildId = null ,buildStatus = null;
		while(flag1){
			buildId = retrieveBuilID(userName, password, hostWithPort);
			if(!previousBuildId.equals(buildId)){
				break;
			}
		}
		if (!buildId.equals("null")) {
			try {
				boolean flag = true;
				while (flag) {
					URL url = new URL("http://" + hostWithPort + "/job/"
							+ jobName + "/" + buildId + "/api/json");
					String authStr = userName + ":" + password;
					String encoding = DatatypeConverter
							.printBase64Binary(authStr.getBytes("utf-8"));

					HttpURLConnection connection = (HttpURLConnection) url
							.openConnection();
					connection.setRequestMethod("GET");
					connection.setDoOutput(true);
					connection.setRequestProperty("Authorization", "Basic "
							+ encoding);
					InputStream content = connection.getInputStream();
					BufferedReader in = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = in.readLine()) != null) {
						// System.out.println("Build No "+buildId+" execution going");
						Pattern pat = Pattern.compile("\"number\":" + buildId
								+ ",.*?\"result\":(.+?),");
						Matcher mat = pat.matcher(line);
						while (mat.find()) {
							buildStatus = mat.group(1).replace("\"", "");
							if (!buildStatus.equals("null")) {
								System.out.println("Buil Mo " + buildId
										+ " execution completed with status:"
										+ buildStatus);
								flag = false;
							}
						}
						break;
					}
				}
			} catch (Exception ex) {
				System.out.println("Exception in retrieveBuildStatus method:"
						+ ex);
				throw ex;
			}
		} else {
			System.out.println("Build ID is null so exiting the execution:");
			// System.exit(0);
		}
		return buildStatus;
	}

	/**
	 * <p>
	 * The method to retrieve the build console based on the build Id
	 * </p>
	 * 
	 * @param userName
	 * @param password
	 * @param hostWithPort
	 * @return
	 * @throws Exception
	 */
	public List<String> retrieveBuildConsole(String userName, String password,
			String hostWithPort) throws Exception {
		List<String> consoleList = new ArrayList<String>();
		/*String buildId = retrieveBuilID(userName, password, hostWithPort,
				"blue", "red", "aborted");*/
		String buildId = retrieveBuilID(userName, password, hostWithPort);
		if (!buildId.equals("null")) {
			try {
				// System.out.println("jobName:" + jobName);
				URL url = new URL("http://" + hostWithPort + "/job/" + jobName
						+ "/" + buildId + "/consoleText");
				String authStr = userName + ":" + password;
				String encoding = DatatypeConverter.printBase64Binary(authStr
						.getBytes("utf-8"));

				HttpURLConnection connection = (HttpURLConnection) url
						.openConnection();
				connection.setRequestMethod("GET");
				connection.setDoOutput(true);
				connection.setRequestProperty("Authorization", "Basic "
						+ encoding);
				InputStream content = connection.getInputStream();
				BufferedReader in = new BufferedReader(new InputStreamReader(
						content));
				String line;
				while ((line = in.readLine()) != null) {
					if (!line.toLowerCase().contains("pipeline"))
						consoleList.add(line);
				}
			} catch (Exception ex) {
				System.out.println("Exception in retrieveBuildConsole method:"
						+ ex);
				throw ex;
			}
		} else {
			System.out.println("Build ID is null so exiting the execution:");
			System.exit(0);
		}
		return consoleList;
	}

	/**
	 * <p>
	 * The method to get the node IP and Label from Jenkin using Java rest api
	 * </p>
	 * 
	 * @param userName
	 * @param password
	 * @param hostWithPort
	 * @return
	 */
	public List<String[]> getNodeIPAndLabel(String userName, String password,
			String hostWithPort) {
		String[] nodeAndLabelArray;
		List<String[]> nodeList = new ArrayList<String[]>();
		try {
			URL url = new URL("http://" + hostWithPort + "/"+ jobName
					+ "/api/json");
			String authStr = userName + ":" + password;
			String encoding = DatatypeConverter.printBase64Binary(authStr
					.getBytes("utf-8"));

			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);
			connection.setRequestProperty("Authorization", "Basic " + encoding);
			InputStream content = connection.getInputStream();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					content));
			String line, line1 = null;
			while ((line = in.readLine()) != null) {
				line1 = line;
			}
			Pattern pat = Pattern
					.compile("\"description\":\"([\\d\\.]+)\",\"displayName\":\"(Slave\\d*)\",");
			Matcher mat = pat.matcher(line1);
			while (mat.find()) {
				nodeAndLabelArray = new String[2];
				nodeAndLabelArray[0] = mat.group(1);
				nodeAndLabelArray[1] = mat.group(2);
				nodeList.add(nodeAndLabelArray);
			}
		} catch (Exception e) {
			System.out.println("Exception in getNodeIPAndLabel method:" + e);
		}
		return nodeList;
	}
}
