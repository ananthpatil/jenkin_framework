public class GetterAndSetter {

	private String jmeterGitPath;
	private String scriptGitPath;
	private String reportCodeGitPath;
	private String jenkinWorkspacepath;
	private String memorySize;
	private String gitBranch;
	private String scriptBranch;
	private String reportBranch;
	private String schedulerTime;
	private String yamlFileName;
	private String modeType;
	private String node1, script1;
	private String node2 = "", script2 = "";
	private String node3 = "", script3 = "";
	private String node4 = "", script4 = "";
	private String node5 = "", script5 = "";
/*	private String jmeterFrameworkJobName;
	private String scriptJobName;
	private String automationJobName;
	private String reportJobName;
	private String pipelineJobName;*/
	private String executionMode;
	private String lebelName;
	

	public void setJmeterGitPath(String jmeterGitPath) {
		this.jmeterGitPath = jmeterGitPath;
	}

	public String getJmeterGitPath() {
		return jmeterGitPath;
	}
	
	public void setJmeterGitBranch(String gitBranch) {
		this.gitBranch = gitBranch;
	}

	public String getJmeterGitBranch() {
		return gitBranch;
	}

	public void setScriptGitPath(String scriptGitPath) {
		this.scriptGitPath = scriptGitPath;
	}

	public String getScriptGitPath() {
		return scriptGitPath;
	}
	
	public void setScriptGitBranch(String scriptBranch) {
		this.scriptBranch = scriptBranch;
	}

	public String getScriptGitBranch() {
		return scriptBranch;
	}

	public void setReptCodeGitPath(String reportCodeGitPath) {
		this.reportCodeGitPath = reportCodeGitPath;
	}

	public String getReptCodeGitPath() {
		return reportCodeGitPath;
	}
	
	public void setReptCodeGitBranch(String reportBranch) {
		this.reportBranch = reportBranch;
	}

	public String getReptCodeGitBranch() {
		return reportBranch;
	}

	public void setJenkinWorkSpacePath(String jenkinWorkspacepath) {
		this.jenkinWorkspacepath = jenkinWorkspacepath;
	}

	public String getJenkinWorkSpacePath() {
		return jenkinWorkspacepath;
	}

	public void setMemorySize(String memorySize) {
		this.memorySize = memorySize;
	}

	public String getMemorySize() {
		return memorySize;
	}
	
	public void setScheduler(String schedulerTime) {
		this.schedulerTime = schedulerTime;
	}

	public String getScheduler() {
		return schedulerTime;
	}
	
	public void setYamlFileName(String yamlFileName) {
		this.yamlFileName = yamlFileName;
	}

	public String getYamlFileName() {
		return yamlFileName;
	}
	
	public void setMode(String modeType) {
		this.modeType = modeType;
	}

	public String getMode() {
		return modeType;
	}

	public void setNode1IP(String node1) {
		this.node1 = node1;
	}

	public String getNode1IP() {
		return node1;
	}

	public void setScript1Name(String script1) {
		this.script1 = script1;
	}

	public String getScript1Name() {
		return script1;
	}

	public void setNode2IP(String node2) {
		this.node2 = node2;
	}

	public String getNode2IP() {
		return node2;
	}

	public void setScript2Name(String script2) {
		this.script2 = script2;
	}

	public String getScript2Name() {
		return script2;
	}

	public void setNode3IP(String node3) {
		this.node3 = node3;
	}

	public String getNode3IP() {
		return node3;
	}

	public void setScript3Name(String script3) {
		this.script3 = script3;
	}

	public String getScript3Name() {
		return script3;
	}

	public void setNode4IP(String node4) {
		this.node4 = node4;
	}

	public String getNode4IP() {
		return node4;
	}

	public void setScript4Name(String script4) {
		this.script4 = script4;
	}

	public String getScript4Name() {
		return script4;
	}

	public void setNode5IP(String node5) {
		this.node5 = node5;
	}

	public String getNode5IP() {
		return node5;
	}

	public void setScript5Name(String script5) {
		this.script5 = script5;
	}

	public String getScript5Name() {
		return script5;
	}
	
	/*public void setJmeterFrameworkJobName(String jmeterFrameworkJobName){
		this.jmeterFrameworkJobName = jmeterFrameworkJobName;
	}
	
	public String getJmeterFrameworkJobName(){
		return jmeterFrameworkJobName;
	}
	
	public void setScriptJobName(String scriptJobName){
		this.scriptJobName = scriptJobName;
	}
	
	public String getScriptJobName(){
		return scriptJobName;
	}
	
	public void setAutomationJobName(String automationJobName){
		this.automationJobName = automationJobName;
	}
	
	public String getAutomationJobName(){
		return automationJobName;
	}
	
	public void setReportJobName(String reportJobName){
		this.reportJobName = scriptJobName;
	}
	
	public String getReportJobName(){
		return reportJobName;
	}
	
	public void setPipelineJobName(String pipelineJobName){
		this.pipelineJobName = pipelineJobName;
	}
	
	public String getPipelineJobName(){
		return pipelineJobName;
	}*/
	
	public void setExecutionMode(String executionMode){
		this.executionMode = executionMode;
	}
	
	public String getExecutionMode(){
		return executionMode;
	}
	
	public void setNodeLabelName(String lebelName){
		this.lebelName = lebelName;
	}
	
	public String getNodeLabelName(){
		return lebelName;
	}
}
