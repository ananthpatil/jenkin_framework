import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class CreateResultFolder {

	private List<String> fileList;
	private String srcFlderPath;

	/***
	 * Constructor to initialize the 
	 * folder path
	 * @param srcFlderPath
	 */
	CreateResultFolder(String srcFlderPath) {
		fileList = new ArrayList<String>();
		this.srcFlderPath = srcFlderPath;
	}

	private void generateFileList(File node) {
		if (node.isFile()) {
			fileList.add(generateZipEntry(node.getAbsoluteFile().toString()));
		}

		if (node.isDirectory()) {
			String[] subNote = node.list();
			for (String filename : subNote) {
				generateFileList(new File(node, filename));
			}
		}
	}

	private String generateZipEntry(String file) {
		return file.substring(srcFlderPath.length() + 1, file.length());
	}

	/**<p> The method to create the 
	 * ZIP folder </p>
	 * @return String
	 */
	public String createZipFolder() {
		byte[] buffer = new byte[1024];
		String rsltPath = null;
		try {
			rsltPath = srcFlderPath + "/" + "results" ;
			File resultFolder = new File(rsltPath);

			if (!resultFolder.exists()) {
				resultFolder.mkdir();
				System.out.println("Result folder created:" + resultFolder);
			} else {
				generateFileList(resultFolder);
				SimpleDateFormat fmt = new SimpleDateFormat();
				fmt.applyPattern("dd-MM-yyyy_hhmm");
				Date curDate = new Date();
				String dateVal = fmt.format(curDate);

				String zipFile = rsltPath + "_" + dateVal + ".zip";
				FileOutputStream fos = new FileOutputStream(zipFile);
				ZipOutputStream zos = new ZipOutputStream(fos);

				for (String file : this.fileList) {

					ZipEntry ze = new ZipEntry(file);
					zos.putNextEntry(ze);

					FileInputStream in = new FileInputStream(srcFlderPath
							+ File.separator + file);

					int len;
					while ((len = in.read(buffer)) > 0) {
						zos.write(buffer, 0, len);
					}

					in.close();
				}
				zos.closeEntry();
				zos.close();
				System.out.println("Zipping the result folder Done");

				for (File file : resultFolder.listFiles()) {
					file.delete();
				}
				resultFolder.mkdir();
			}
		} catch (IOException ex) {
			System.out.println("Exception in create zip folder method:"+ex);
		} catch (Exception e) {
			System.out.println("Exception in create zip folder method:"+e);
		}
		return rsltPath;
	}
}
