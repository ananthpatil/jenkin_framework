import java.util.List;

import javax.servlet.ServletContext;

final class IPMapping_BuildJmeterAndScriptJobs {

	private String jenkinUserName, jenkinPassword, jenkinHost, mode;
	private List<String[]> nodeList;
	private List<GetterAndSetter> getterSetterList;
	private String jmeterFrameworkJobName, scriptJobName;
	private String multiJobName, multiJobParentName, serverIP;
	private String nodeArray[];
	private int total, scriptIndex = 0;
	private String multiJobStatus = "dummy";

	GetterAndSetter setterObj;
	JobConfigurationReading configObj;
	ServletContext context;

	public String getJobStatus() {
		return multiJobStatus;
	}

	public int getScriptIndex() {
		return scriptIndex;
	}

	/**
	 * <p>
	 * Constructor to initialize the objects
	 * </p>
	 * 
	 * @param jenkinUserName
	 * @param jenkinPassword
	 * @param jenkinHost
	 * @param setterObj
	 * @param configObj
	 * @param context
	 * @param getterSetterList
	 * @param mode
	 */
	IPMapping_BuildJmeterAndScriptJobs(String jenkinUserName,
			String jenkinPassword, String jenkinHost,
			GetterAndSetter setterObj, JobConfigurationReading configObj,
			ServletContext context, List<GetterAndSetter> getterSetterList,
			String mode) {

		this.setterObj = setterObj;
		this.configObj = configObj;
		this.jenkinUserName = jenkinUserName;
		this.jenkinPassword = jenkinPassword;
		this.jenkinHost = jenkinHost;
		this.context = context;
		this.getterSetterList = getterSetterList;
		this.mode = mode;
		getNodeListFromJenkin(jenkinUserName, jenkinPassword, jenkinHost);
		getNodeIpOfUser();
		getContextParameters();
	}

	/**
	 * <p>
	 * Method to fetch the IP address and label of nodes from Jenkin
	 * </p>
	 * 
	 * @param jenkinUserName
	 * @param jenkinPassword
	 * @param jenkinHost
	 * @return List
	 */
	private void getNodeListFromJenkin(String jenkinUserName,
			String jenkinPassword, String jenkinHost) {
		BuildJob nodeObject = new BuildJob("computer");
		nodeList = nodeObject.getNodeIPAndLabel(jenkinUserName, jenkinPassword,
				jenkinHost);
	}

	/**
	 * <p>
	 * Method to read the IP address which are entered in UI and store each one
	 * in separate variable
	 * </p>
	 */
	private void getNodeIpOfUser() {
		String node1, node2, node3, node4, node5;
		GetterAndSetter getterSetter = getterSetterList.get(0);
		node1 = getterSetter.getNode1IP();
		node2 = getterSetter.getNode2IP();
		node3 = getterSetter.getNode3IP();
		node4 = getterSetter.getNode4IP();
		node5 = getterSetter.getNode5IP();
		nodeArray = new String[] { "master", node1, node2, node3, node4, node5 };
		serverIP = jenkinHost.substring(0, jenkinHost.indexOf(":"));
		if (mode.equalsIgnoreCase("distributed"))
			total = nodeArray.length;
		else
			total = 1;
	}

	/**
	 * <p>
	 * The method to get the JOB names which are configured in web.xml
	 * <p>
	 */
	private void getContextParameters() {
		jmeterFrameworkJobName = context
				.getInitParameter("jmeterFrameworkJobName");
		scriptJobName = context.getInitParameter("scriptJobName");
		multiJobName = context.getInitParameter("multiJobName");
		multiJobParentName = context.getInitParameter("multiJobParentName");
	}

	/**
	 * <p>
	 * The method to map the user entered IP with node IP's of Jenkin based on
	 * that map method will trigger both Jmeter and script jobs
	 * </p>
	 * 
	 * @return
	 * @throws Exception
	 */
	public void nodeIpMapping() throws Exception {

		for (int n = 0; n < total; n++) {
			boolean ipFound = false;
			String value = nodeArray[n];
			String hostName = null;
			if (value.length() != 0) {
				if (n == 0) {
					hostName = "master";
				} else {
					for (int nl = 0; nl < nodeList.size(); nl++) {
						String[] array = nodeList.get(nl);
						if (value.equals(array[0].trim())) {
							hostName = array[1].trim();
							ipFound = true;
							break;
						}
					}
					if (serverIP.equals(value)) {
						scriptIndex = n;
					}
					if (ipFound == false || !serverIP.equals(value)) {
						switch (n) {
						case 1:
							setterObj.setScript1Name("");
							break;
						case 2:
							setterObj.setScript2Name("");
							break;
						case 3:
							setterObj.setScript3Name("");
							break;
						case 4:
							setterObj.setScript4Name("");
							break;
						case 5:
							setterObj.setScript5Name("");
							break;
						}
					}
					// hostName = nodeName + n;
				}

				if (ipFound == true || n == 0) {
					BuildJob multiJobObj1 = new BuildJob(multiJobName);
					String previousBuildId = multiJobObj1.retrieveBuilID(
							jenkinUserName, jenkinPassword, jenkinHost);

					configObj.readExistingJmeterJobConfig(jenkinUserName,
							jenkinPassword, jenkinHost, jmeterFrameworkJobName,
							hostName);

					configObj
							.readExistingScriptJobConfig(jenkinUserName,
									jenkinPassword, jenkinHost, scriptJobName,
									hostName);
					BuildJob multiJobObj = new BuildJob(multiJobParentName);
					multiJobObj.execeuteJob(jenkinUserName, jenkinPassword,
							jenkinHost);
					Thread.sleep(10000);

					BuildJob multiJobObj2 = new BuildJob(multiJobName);
					multiJobStatus = multiJobObj2.retrieveBuildStatus(
							jenkinUserName, jenkinPassword, jenkinHost,
							previousBuildId);
					Thread.sleep(10000);
				}
			}
		}

	}
}
