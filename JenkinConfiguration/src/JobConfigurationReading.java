import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class JobConfigurationReading extends ReadTaurusConfig {
	
    final byte[] BUFFERSIZE = new byte[8192];
	private String fileName = "config.xml";
	//GetterAndSetter getterSetter = getterSetterList.get(0);

	
	/**
	 * <p> Constructor </p>
	 * @param getterSetterList
	 * @param scriptPath
	 * @param jmeterPath
	 * @param slaveWorkSpacePath
	 */
	public JobConfigurationReading(List<GetterAndSetter> getterSetterList,
			String scriptPath, String jmeterPath, String slaveWorkSpacePath,
			String jenkinHost) {
		super(getterSetterList,scriptPath,jmeterPath,slaveWorkSpacePath,jenkinHost);
	}

	/**
	 * <p> The method used to read and update the
	 *     config.xml file of apache_jmeter
	 *     job by connecting the Jenkin server via REST API </p>
	 *     @return void
	 *     @param
	 */
	public void readExistingJmeterJobConfig(String userName, String password,
			String hostWithPort,String jmeterFrameworkJobName,String hostName) {
		try {
			String authStr = userName + ":" + password;
			String encoding = DatatypeConverter.printBase64Binary(authStr
					.getBytes("utf-8"));

			URL url = new URL("http://" + hostWithPort + "/job/" + jmeterFrameworkJobName
					+ "/" + fileName);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Authorization", "Basic " + encoding);

			InputStream content = connection.getInputStream();
			String saveFilePath = System.getProperty("user.home")
					+ File.separator + "config1.xml";
			FileOutputStream outputStream = new FileOutputStream(saveFilePath);

			BufferedReader in = new BufferedReader(new InputStreamReader(
					content));
			String line;
			while ((line = in.readLine()) != null) {
			    line = line.replace("xml version='1.1'", "xml version='1.0'");
				outputStream.write(line.getBytes());
				outputStream.write("\n".getBytes());

				if (line.toUpperCase().contains(
						"<NAME>JMETER_GIT_PATH</NAME>")) {
					try {
						String line1 = in.readLine();
						outputStream.write(line1.getBytes());
						outputStream.write("\n".getBytes());

						String line2 = in.readLine();
						line2 = line2.replace(
								line2.substring(line2.indexOf(">") + 1,
										line2.indexOf("</")), getterSetter.getJmeterGitPath());
						outputStream.write(line2.getBytes());
						outputStream.write("\n".getBytes());
					} catch (Exception e) {
					}
				}
				if (line.toUpperCase().contains("<NAME>JMETER_GIT_BRANCH</NAME>")) {
					try {
						String line1 = in.readLine();
						outputStream.write(line1.getBytes());
						outputStream.write("\n".getBytes());

						String line2 = in.readLine();
						line2 = line2.replace(
								line2.substring(line2.indexOf(">") + 1,
										line2.indexOf("</")), getterSetter.getJmeterGitBranch());
						outputStream.write(line2.getBytes());
						outputStream.write("\n".getBytes());
					} catch (Exception e) {
					}
				}
				if (line.toLowerCase().contains("<defaultslaves>")) {
					try {
						String line1 = in.readLine();
						line1 = line1.replace(line1.substring(
								line1.indexOf(">") + 1,
								line1.indexOf("</")),hostName);
						outputStream.write(line1.getBytes());
						outputStream.write("\n".getBytes());
					/*if(getterSetter.getExecutionMode().equalsIgnoreCase(
							"distributed")){
						String[] nodeArray = { node1, node2, node3, node4,
								node5 };
						for (int n = 0; n < nodeArray.length; n++) {
							if (nodeArray[n].length() != 0) {
								String line2 = line1.replace(line1.substring(
										line1.indexOf(">") + 1,
										line1.indexOf("</")),
										getterSetter.getNodeLabelName()
												+ (n + 1));
								outputStream.write(line2.getBytes());
								outputStream.write("\n".getBytes());
							}
						}
					  }*/
					} catch (Exception e) {
					}
				}
			}
			outputStream.close();
			content.close();
			updateJobConfig(userName, password, hostWithPort,jmeterFrameworkJobName,saveFilePath);
		} catch (Exception e) {
			System.out.println("Exception in readExistingJmeterJobConfig method:"+e);
		}
	}
	
	/**
	 * <p> The method used to read and update the
	 *     config.xml file of auto_perf_jmeter_scripts
	 *     job by connecting the Jenkin server via REST API </p>
	 *     @return void
	 *     @param
	 */
	public void readExistingScriptJobConfig(String userName, String password,
			String hostWithPort,String scriptJobName,String hostName) {
		try {
			//GetterAndSetter getterSetter = getterSetterList.get(0);

			String authStr = userName + ":" + password;
			String encoding = DatatypeConverter.printBase64Binary(authStr
					.getBytes("utf-8"));

			URL url = new URL("http://" + hostWithPort + "/job/" + scriptJobName
					+ "/" + fileName);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Authorization", "Basic " + encoding);

			InputStream content = connection.getInputStream();
			String saveFilePath = System.getProperty("user.home")
					+ File.separator + "config2.xml";
			FileOutputStream outputStream = new FileOutputStream(saveFilePath);

			BufferedReader in = new BufferedReader(new InputStreamReader(
					content));
			String line;
			while ((line = in.readLine()) != null) {
				line = line.replace("xml version='1.1'", "xml version='1.0'");
				outputStream.write(line.getBytes());
				outputStream.write("\n".getBytes());

				if (line.toUpperCase().contains("<NAME>SCRIPT_GIT_PATH</NAME>")) {
					try {
						String line1 = in.readLine();
						outputStream.write(line1.getBytes());
						outputStream.write("\n".getBytes());

						String line2 = in.readLine();
						line2 = line2.replace(
								line2.substring(line2.indexOf(">") + 1,
										line2.indexOf("</")),
								getterSetter.getScriptGitPath());
						outputStream.write(line2.getBytes());
						outputStream.write("\n".getBytes());
					} catch (Exception e) {
					}
				}
				if (line.toUpperCase().contains(
						"<NAME>SCRIPT_GIT_BRANCH</NAME>")) {
					try {
						String line1 = in.readLine();
						outputStream.write(line1.getBytes());
						outputStream.write("\n".getBytes());

						String line2 = in.readLine();
						line2 = line2.replace(
								line2.substring(line2.indexOf(">") + 1,
										line2.indexOf("</")),
								getterSetter.getScriptGitBranch());
						outputStream.write(line2.getBytes());
						outputStream.write("\n".getBytes());
					} catch (Exception e) {
					}
				}
				if (line.toLowerCase().contains("<defaultslaves>")) {
					try {
						String line1 = in.readLine();
						line1 = line1.replace(line1.substring(
								line1.indexOf(">") + 1,
								line1.indexOf("</")),hostName);
						outputStream.write(line1.getBytes());
						outputStream.write("\n".getBytes());
					/* if(getterSetter.getExecutionMode().equalsIgnoreCase(
								"distributed")){
						String[] nodeArray = { node1, node2, node3, node4,
								node5 };
						for (int n = 0; n < nodeArray.length; n++) {
							if (nodeArray[n].length() != 0) {
								String line2 = line1.replace(line1.substring(
										line1.indexOf(">") + 1,
										line1.indexOf("</")),
										getterSetter.getNodeLabelName()
												+ (n + 1));
								outputStream.write(line2.getBytes());
								outputStream.write("\n".getBytes());
							}
						}
					  }*/
					} catch (Exception e) {
					}
				}
			 }
			outputStream.close();
			content.close();
			updateJobConfig(userName, password, hostWithPort, scriptJobName, saveFilePath);
		} catch (Exception e) {
			System.out.println("Exception in readExistingScriptJobConfig method:"+e);
		}
	}
	
	/**
	 * <p> The method used to read and update the
	 *     config.xml file of Jmeter_Automation
	 *     job by connecting the Jenkin server via REST API </p>
	 *     @return void
	 *     @param
	 */
	public void readExistingJmeterAutomationJobConfig(String userName,
			String password, String hostWithPort, String newYamlFilePath,
			String resultPath, String automationJobNamae) {
		try {
			//GetterAndSetter getterSetter = getterSetterList.get(0);

			String authStr = userName + ":" + password;
			String encoding = DatatypeConverter.printBase64Binary(authStr
					.getBytes("utf-8"));

			URL url = new URL("http://" + hostWithPort + "/job/" + automationJobNamae
					+ "/" + fileName);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Authorization", "Basic " + encoding);

			InputStream content = connection.getInputStream();
			String saveFilePath = System.getProperty("user.home")
					+ File.separator + "config3.xml";
			FileOutputStream outputStream = new FileOutputStream(saveFilePath);

			BufferedReader in = new BufferedReader(new InputStreamReader(
					content));
			String line;
			while ((line = in.readLine()) != null) {
				line = line.replace("xml version='1.1'", "xml version='1.0'");
				outputStream.write(line.getBytes());
				outputStream.write("\n".getBytes());

				if (line.toUpperCase().contains(
						"<NAME>RESULT_FOLDER_PATH</NAME>")) {
					try {
						String line1 = in.readLine();
						outputStream.write(line1.getBytes());
						outputStream.write("\n".getBytes());

						String line2 = in.readLine();
						line2 = line2.replace(
								line2.substring(line2.indexOf(">") + 1,
										line2.indexOf("</")), resultPath+"/");
						outputStream.write(line2.getBytes());
						outputStream.write("\n".getBytes());
					} catch (Exception e) {
					}
				}
				if (line.toUpperCase().contains("<NAME>YAML_FILE_PATH</NAME>")) {
					try {
						String line1 = in.readLine();
						outputStream.write(line1.getBytes());
						outputStream.write("\n".getBytes());

						String line2 = in.readLine();
						line2 = line2.replace(
								line2.substring(line2.indexOf(">") + 1,
										line2.indexOf("</")),
								newYamlFilePath + "/"
										+ getterSetter.getYamlFileName());
						outputStream.write(line2.getBytes());
						outputStream.write("\n".getBytes());
					} catch (Exception e) {
					}
				}
			}

			outputStream.close();
			content.close();
			updateJobConfig(userName, password, hostWithPort, automationJobNamae, saveFilePath);
		} catch (Exception e) {
			System.out.println("Exception in readExistingJmeterAutomationJobConfig method:"+e);
		}
	}
	
	
	/**
	 * <p> The method used to read and update the
	 *     config.xml file of Generate_Report
	 *     job by connecting the Jenkin server via REST API </p>
	 *     @return void
	 *     @param
	 */
	public void readExistingReportJobConfig(String userName, String password,
			String hostWithPort,String resultFolderPath,String reportJobName) {
		try {
			//GetterAndSetter getterSetter = getterSetterList.get(0);

			String authStr = userName + ":" + password;
			String encoding = DatatypeConverter.printBase64Binary(authStr
					.getBytes("utf-8"));

			URL url = new URL("http://" + hostWithPort + "/job/" + reportJobName
					+ "/" + fileName);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Authorization", "Basic " + encoding);

			InputStream content = connection.getInputStream();
			String saveFilePath = System.getProperty("user.home")
					+ File.separator + "config4.xml";
			FileOutputStream outputStream = new FileOutputStream(saveFilePath);

			BufferedReader in = new BufferedReader(new InputStreamReader(
					content));
			String line;
			while ((line = in.readLine()) != null) {
				 line = line.replace("xml version='1.1'", "xml version='1.0'");
				outputStream.write(line.getBytes());
				outputStream.write("\n".getBytes());

				if (line.toUpperCase().contains(
						"<NAME>REPORT_GIT_PATH</NAME>")) {
					try {
						String line1 = in.readLine();
						outputStream.write(line1.getBytes());
						outputStream.write("\n".getBytes());

						String line2 = in.readLine();
						line2 = line2.replace(
								line2.substring(line2.indexOf(">") + 1,
										line2.indexOf("</")), getterSetter.getReptCodeGitPath());
						outputStream.write(line2.getBytes());
						outputStream.write("\n".getBytes());
					} catch (Exception e) {
					}
				}
				if (line.toUpperCase().contains("<NAME>REPORT_GIT_BRANCH</NAME>")) {
					try {
						String line1 = in.readLine();
						outputStream.write(line1.getBytes());
						outputStream.write("\n".getBytes());

						String line2 = in.readLine();
						line2 = line2.replace(
								line2.substring(line2.indexOf(">") + 1,
										line2.indexOf("</")), getterSetter.getReptCodeGitBranch());
						outputStream.write(line2.getBytes());
						outputStream.write("\n".getBytes());
					} catch (Exception e) {
					}
				}
				if (line.toUpperCase().contains("<NAME>RESULT_FOLDER_PATH</NAME>")) {
					try {
						String line1 = in.readLine();
						outputStream.write(line1.getBytes());
						outputStream.write("\n".getBytes());

						String line2 = in.readLine();
						line2 = line2.replace(
								line2.substring(line2.indexOf(">") + 1,
										line2.indexOf("</")), resultFolderPath);
						outputStream.write(line2.getBytes());
						outputStream.write("\n".getBytes());
					} catch (Exception e) {
					}
				}
			}

			outputStream.close();
			content.close();
            updateJobConfig(userName, password, hostWithPort, reportJobName, saveFilePath);
		} catch (Exception e) {
			System.out.println("Exception in readExistingReportJobConfig method:"+e);
		}
	}
	
	
	/**
	 * <p>The method to POST the updated configuration
	 *    file to the server with respective its job</p> 
	 * @param userName
	 * @param password
	 * @param hostWithPort
	 * @param jobName
	 * @param filePath
	 * @return void
	 */
	private void updateJobConfig(String userName, String password,
			String hostWithPort, String jobName, String filePath) {
		final byte[] BUFFERSIZE = new byte[8192];
		try {
			URL url = new URL("http://" + hostWithPort + "/job/" + jobName
					+ "/" + fileName);
			String authStr = userName + ":" + password;
			String encoding = DatatypeConverter.printBase64Binary(authStr
					.getBytes("utf-8"));

			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setReadTimeout(10000);
			connection.setConnectTimeout(15000);
			connection.setRequestMethod("POST");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setRequestProperty("Authorization", "Basic " + encoding);
			FileInputStream inputStream = new FileInputStream(
					new File(filePath));
			
			try (DataOutputStream wr = new DataOutputStream(
					connection.getOutputStream())) {
				int c;
				while ((c = inputStream.read(BUFFERSIZE, 0, BUFFERSIZE.length)) > 0) {
					wr.write(BUFFERSIZE, 0, c);
				}
			 }
			InputStream content = connection.getInputStream();
			BufferedReader in = new BufferedReader(new InputStreamReader (content));
			String line;
			while ((line = in.readLine()) != null) {
				System.out.println(line);
			}
			inputStream.close();
		} catch (Exception e) {
			System.out.println("Exception in updateJobConfig method:"+e);
		}
	}
	
	/**
	 * <p> The method used to read and update the
	 *     config.xml file of apache_jmeter
	 *     job by using DOM parser</p>
	 *     @return void
	 *     @param
	 */
	public void readExistingJmeterJobConfig(String xmlfilePath){
		String saveFilePath = xmlfilePath + File.separator + "jobs"
				+ File.separator + "apache_jmeter" + File.separator + fileName;
		File xmlFile = new File(saveFilePath);
		Document doc = null;
		if(xmlFile.exists()){
			try{
				//GetterAndSetter getterSetter = getterSetterList.get(0);
				DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder =  dFactory.newDocumentBuilder();
				doc = dBuilder.parse(xmlFile);

				doc.getDocumentElement().normalize();
				System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

				Node paramNode = doc.getElementsByTagName("parameterDefinitions").item(0);
				NodeList childList = paramNode.getChildNodes();
				for(int n=0;n<childList.getLength();n++){
					if(childList.item(n).getNodeType() != Node.TEXT_NODE){
						NodeList paramChilds = childList.item(n).getChildNodes();	
						for(int p=0;p<paramChilds.getLength();p++){
							if(paramChilds.item(p).getNodeType() == Node.ELEMENT_NODE){
								String textContent = paramChilds.item(p).getTextContent();
								if(textContent.toUpperCase().equals("JMETER_GIT_PATH")){
									paramChilds.item(p+3).getNextSibling().setTextContent(getterSetter.getJmeterGitPath());
									break;
								}
								if(textContent.toUpperCase().equals("JMETER_GIT_BRANCH")){
									paramChilds.item(p+3).getNextSibling().setTextContent(getterSetter.getJmeterGitBranch());
									break;
								}
							}
						}
					}
				}
				updateXmlFile(doc, saveFilePath);
			  }catch(ParserConfigurationException p){
				  System.out.println("Exception in readExistingJmeterAutomationJobConfig:"+p);
			  }
			   catch(Exception e){
				   System.out.println("Exception in readExistingJmeterAutomationJobConfig:"+e);
			  }
		  }
	   }
	
	/**
	 * <p> The method used to read and update the
	 *     config.xml file of auto_perf_jmeter_scripts
	 *     job by using DOM parser  </p>
	 *     @return void
	 *     @param
	 */
	public void readExistingScriptJobConfig(String xmlfilePath){
		String saveFilePath = xmlfilePath + File.separator + "jobs"
				+ File.separator + "auto_perf_jmeter_scripts" + File.separator + fileName;
		File xmlFile = new File(saveFilePath);
		Document doc = null;
		if(xmlFile.exists()){
			try{
				//GetterAndSetter getterSetter = getterSetterList.get(0);
				DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder =  dFactory.newDocumentBuilder();
				doc = dBuilder.parse(xmlFile);

				doc.getDocumentElement().normalize();
				System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

				Node paramNode = doc.getElementsByTagName("parameterDefinitions").item(0);
				NodeList childList = paramNode.getChildNodes();
				for(int n=0;n<childList.getLength();n++){
					if(childList.item(n).getNodeType() != Node.TEXT_NODE){
						NodeList paramChilds = childList.item(n).getChildNodes();	
						for(int p=0;p<paramChilds.getLength();p++){
							if(paramChilds.item(p).getNodeType() == Node.ELEMENT_NODE){
								String textContent = paramChilds.item(p).getTextContent();
								if(textContent.toUpperCase().equals("SCRIPT_GIT_PATH")){
									paramChilds.item(p+3).getNextSibling().setTextContent(getterSetter.getScriptGitPath());
									break;
								}
								if(textContent.toUpperCase().equals("SCRIPT_GIT_BRANCH")){
									paramChilds.item(p+3).getNextSibling().setTextContent(getterSetter.getScriptGitBranch());
									break;
								}
							}
						}
					}
				}
				updateXmlFile(doc, saveFilePath);
			  }catch(ParserConfigurationException p){
				  System.out.println("Exception in readExistingScriptJobConfig:"+p);
			  }
			   catch(Exception e){
				   System.out.println("Exception in readExistingScriptJobConfig:"+e);
			  }
		  }
	   }
	

	/**
	 * <p> The method used to read and update the
	 *     config.xml file of Jmeter_Automation
	 *     job by using DOM parser  </p>
	 *     @return void
	 *     @param
	 */
	public void readExistingJmeterAutomationJobConfig(String xmlfilePath){
		String saveFilePath = xmlfilePath + File.separator + "jobs"
				+ File.separator + "Jmeter_Automation" + File.separator + fileName;
		File xmlFile = new File(saveFilePath);
		Document doc = null;
		if(xmlFile.exists()){
			try{
				//GetterAndSetter getterSetter = getterSetterList.get(0);
				DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder =  dFactory.newDocumentBuilder();
				doc = dBuilder.parse(xmlFile);

				doc.getDocumentElement().normalize();
				System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

				Node paramNode = doc.getElementsByTagName("parameterDefinitions").item(0);
				NodeList childList = paramNode.getChildNodes();
				for(int n=0;n<childList.getLength();n++){
					if(childList.item(n).getNodeType() != Node.TEXT_NODE){
						NodeList paramChilds = childList.item(n).getChildNodes();	
						for(int p=0;p<paramChilds.getLength();p++){
							if(paramChilds.item(p).getNodeType() == Node.ELEMENT_NODE){
								String textContent = paramChilds.item(p).getTextContent();
								if(textContent.toUpperCase().equals("RESULT_FOLDER_PATH")){
									paramChilds.item(p+3).getNextSibling().setTextContent("D:/C4C/");
									break;
								}
								if(textContent.toUpperCase().equals("YAML_FILE_PATH")){
									paramChilds.item(p+3).getNextSibling().setTextContent(getterSetter.getJenkinWorkSpacePath());
									break;
								}
							}
						}
					}
				}
				updateXmlFile(doc, saveFilePath);
			  }catch(ParserConfigurationException p){
				  System.out.println("Exception in readExistingJmeterAutomationJobConfig:"+p);
			  }
			   catch(Exception e){
				   System.out.println("Exception in readExistingJmeterAutomationJobConfig:"+e);
			  }
		  }
	   }
	
	
	/**
	 * <p> The method used to read and update the
	 *     config.xml file of Generate_Report
	 *     job by using DOM parser  </p>
	 *     @return void
	 *     @param
	 */
	public void readExistingReportJobConfig(String xmlfilePath){
		String saveFilePath = xmlfilePath + File.separator + "jobs"
				+ File.separator + "Generate_Report" + File.separator + fileName;
		File xmlFile = new File(saveFilePath);
		Document doc = null;
		if(xmlFile.exists()){
			try{
				//GetterAndSetter getterSetter = getterSetterList.get(0);
				DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder =  dFactory.newDocumentBuilder();
				doc = dBuilder.parse(xmlFile);

				doc.getDocumentElement().normalize();
				System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

				Node paramNode = doc.getElementsByTagName("parameterDefinitions").item(0);
				NodeList childList = paramNode.getChildNodes();
				for(int n=0;n<childList.getLength();n++){
					if(childList.item(n).getNodeType() != Node.TEXT_NODE){
						NodeList paramChilds = childList.item(n).getChildNodes();	
						for(int p=0;p<paramChilds.getLength();p++){
							if(paramChilds.item(p).getNodeType() == Node.ELEMENT_NODE){
								String textContent = paramChilds.item(p).getTextContent();
								if(textContent.toUpperCase().equals("REPORT_GIT_PATH")){
									paramChilds.item(p+3).getNextSibling().setTextContent(getterSetter.getReptCodeGitPath());
									break;
								}
								if(textContent.toUpperCase().equals("REPORT_GIT_BRANCH")){
									paramChilds.item(p+3).getNextSibling().setTextContent(getterSetter.getReptCodeGitBranch());
									break;
								}
							}
						}
					}
				}
				updateXmlFile(doc, saveFilePath);
			  }catch(ParserConfigurationException p){
				  System.out.println("Exception in readExistingReportJobConfig:"+p);
			  }
			   catch(Exception e){
				   System.out.println("Exception in readExistingReportJobConfig:"+e);
			  }
		  }
	   }
	
	/**
	 * <p> The method to update the
	 *     Given XML file
	 *     </p>
	 *  @return void
	 *  @param
	 */
	public void updateXmlFile(Document doc,String xmlFieName){
		try {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(xmlFieName);
			transformer.transform(source, result);
		} catch (TransformerConfigurationException e1) {
			System.out.println("Exception in updateXmlFile method:"+e1);
		}  catch (TransformerException e) {
			System.out.println("Exception in updateXmlFile method:"+e);
		}
	}
}
