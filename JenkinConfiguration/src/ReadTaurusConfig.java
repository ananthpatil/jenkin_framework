import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ReadTaurusConfig {

	private List<String> list;
	private int scriptIndex;
	// protected List<GetterAndSetter> getterSetterList;
	private String fileNameToSearch, slaveWorkSpacePath;
	private String scriptPath, jmeterPath;
	protected GetterAndSetter getterSetter;
	protected String node1, node2, node3, node4, node5;
	protected String script1, script2, script3, script4, script5;
	private String jenkinHost;

	String scenarioTemplate = "  - scenario: scenarioOne";
	String executionMode = "    distributed: ";
	String ipAddress = "    - systemIP";
	String scenarios = "   scenarioOne: ";
	String scriptTemplate = "     script: \"${script_path}scriptName\"";

	/**
	 * <p>
	 * The method to get the getter setter object
	 * </p>
	 * 
	 * @param getterSetterList
	 */
	/*
	 * public void intializeList(List<GetterAndSetter> getterSetterList,String
	 * scriptPath) { this.getterSetterList = getterSetterList; this.scriptPath =
	 * scriptPath; }
	 */
	/**
	 * <p>
	 * Constructor
	 * </p>
	 * 
	 * @param getterSetterList
	 * @param scriptPath
	 * @param slaveWorkSpacePath
	 */
	ReadTaurusConfig(List<GetterAndSetter> getterSetterList, String scriptPath,
			String jmeterPath, String slaveWorkSpacePath, String jenkinHost) {
		this.getterSetter = getterSetterList.get(0);
		this.scriptPath = scriptPath;
		this.jmeterPath = jmeterPath;
		this.slaveWorkSpacePath = slaveWorkSpacePath;
		this.jenkinHost = jenkinHost;
		// this.getterSetter = this.getterSetterList.get(0);
		intilizeNodeAndScriptVariables();
	}

	/**
	 * <p>
	 * the method to initialize the node and script variables
	 * </p>
	 * 
	 * @return void
	 */
	private void intilizeNodeAndScriptVariables() {
		node1 = getterSetter.getNode1IP();
		node2 = getterSetter.getNode2IP();
		node3 = getterSetter.getNode3IP();
		node4 = getterSetter.getNode4IP();
		node5 = getterSetter.getNode5IP();

		script1 = getterSetter.getScript1Name();
		script2 = getterSetter.getScript2Name();
		script3 = getterSetter.getScript3Name();
		script4 = getterSetter.getScript4Name();
		script5 = getterSetter.getScript5Name();
	}

	public String getFileNameToSearch() {
		return fileNameToSearch;
	}

	public void setFileNameToSearch(String fileNameToSearch) {
		this.fileNameToSearch = fileNameToSearch;
	}

	/**
	 * <p>
	 * The method to read the YAML template file from the specified path
	 * </p>
	 * 
	 * @param yamlFileName
	 * @throws IOException
	 */
	public String readConfigTemplate(String yamlTemplatePath,
			String yamlTemplateFileName, String newYamlFilePath, int index)
			throws IOException {
		String line;
		scriptIndex = index;
		list = new ArrayList<String>();
		BufferedReader reader = null;
		try {
			/*
			 * yamlTemplatePath = yamlTemplatePath.replace(yamlTemplatePath
			 * .substring(yamlTemplatePath.indexOf(".metadata"),
			 * yamlTemplatePath.indexOf("JenkinConfiguration")), "");
			 */
			yamlTemplatePath = yamlTemplatePath
					.replace(
							"\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps",
							"");
			File yamlFilname = new File(yamlTemplatePath + yamlTemplateFileName);
			reader = new BufferedReader(new FileReader(yamlFilname));
			while ((line = reader.readLine()) != null) {
				list.add(line);
			}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
		String filePath = createYamlConfig(list, yamlTemplateFileName,
				newYamlFilePath);
		return filePath;
	}

	/**
	 * <p>
	 * The method to search the file recursively in all folder
	 * </p>
	 * 
	 * @param file
	 * @return
	 */
	private String searchScript(File file) {
		String scriptName = "";
		try {
			if (file.isDirectory()) {
				if (file.canRead()) {
					for (File temp : file.listFiles()) {
						if (temp.isDirectory()) {
							scriptName = searchScript(temp);
							if (scriptName.length() != 0) {
								return scriptName;
							}
						} else {
							if (getFileNameToSearch().trim().equalsIgnoreCase(
									temp.getName().trim())) {
								scriptName = temp.getAbsolutePath();
								return scriptName;
							}
						}
					}
				} else {
					System.out.println(file.getAbsoluteFile()
							+ "Permission Denied");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return scriptName;
	}

	/**
	 * <p>
	 * The following method to replace the path for the given script
	 * </p>
	 * 
	 * @return String
	 */
	private String searchedScriptReplacePath(String script) {
		File file = new File(scriptPath);
		if (!(script.contains("/") || script.contains("\\"))
				&& script.length() != 0) {
			setFileNameToSearch(script);
			script = searchScript(file).replace("\\", "/");
			script = script.replace(scriptPath, "");
		}
		return script;
	}

	/**
	 * <p>
	 * The method to create the new YAML file
	 * </p>
	 * 
	 * @param yamlTemplatelist
	 * @throws IOException
	 */
	private String createYamlConfig(List<String> yamlTemplatelist,
			String yamlTemplateFileName, String newYamlFilePath)
			throws IOException {
		BufferedWriter writer = null;
		String resultPath = null;
		try {
			// GetterAndSetter getterSetter = getterSetterList.get(0);
			// String workspacePath = object.getJenkinWorkSpacePath();
			File yamlFilname = new File(newYamlFilePath + File.separator
					+ getterSetter.getYamlFileName());

			writer = new BufferedWriter(new FileWriter(yamlFilname));
			String memorySize = getterSetter.getMemorySize();

			script1 = searchedScriptReplacePath(script1);
			script2 = searchedScriptReplacePath(script2);
			script3 = searchedScriptReplacePath(script3);
			script4 = searchedScriptReplacePath(script4);
			script5 = searchedScriptReplacePath(script5);

			String[] arrayScripts = { script1, script2, script3, script4,
					script5 };
			for (int a = 0; a < arrayScripts.length; a++) {
				if (arrayScripts[a].length() != 0) {
					File filval = new File(arrayScripts[a].trim());
					resultPath = filval.getParentFile().getParent()
							.replace("\\", "/");
					break;
				}
			}

			if (resultPath != null) {
				for (int j = 0; j < yamlTemplatelist.size(); j++) {
					String key = yamlTemplatelist.get(j);
					String key1 = key;

					if (key.contains("memorysize"))
						key = key.replace("memorysize", memorySize);

					if (key.contains("systemIP"))
						key = key.replace("systemIP", node1);

					if (key.contains("scriptName"))
						key = key.replace("scriptName", script1);

					if (key.contains("SCRIPTPATH"))
						key = key.replace("SCRIPTPATH", scriptPath);

					if (key.contains("JMETERPATH"))
						key = key.replace("JMETERPATH", jmeterPath);

					if (key.contains("RMIHOSTNAME")) {
						String serverIP = jenkinHost.substring(0,
								jenkinHost.indexOf(":"));
						key = key.replace("RMIHOSTNAME", serverIP);
					}

					writer.write(key);
					writer.write("\n");

					if (key1.contains("systemIP")
							&& yamlTemplateFileName.contains("distributed")) {
						String nodeArray[] = { node2, node3, node4, node5 };
						String scenarioArray[] = { "scenarioTwo",
								"scenarioThree", "scenarioFour", "scenarioFive" };
						for (int n = 0; n < nodeArray.length; n++) {
							if (nodeArray[n].length() != 0) {
								writer.write(scenarioTemplate.replace(
										"scenarioOne", scenarioArray[n] + "\n"));
								writer.write(executionMode + "\n");
								writer.write(ipAddress.replace("systemIP",
										nodeArray[n]) + "\n");
							}
						}
					} else if (key1.contains("- scenario")
							&& yamlTemplateFileName.contains("standalone")) {
						String nodeArray[] = { node2, node3, node4, node5 };
						String scenarioArray[] = { "scenarioTwo",
								"scenarioThree", "scenarioFour", "scenarioFive" };
						for (int n = 0; n < nodeArray.length; n++) {
							if (nodeArray[n].length() != 0) {
								writer.write(scenarioTemplate.replace(
										"scenarioOne", scenarioArray[n] + "\n"));
							}
						}
					}
					if (key1.contains("scriptName")) {
						String scriptArray[] = { script2, script3, script4,
								script5 };
						String scenarioArray[] = { "scenarioTwo",
								"scenarioThree", "scenarioFour", "scenarioFive" };
						for (int n = 0; n < scriptArray.length; n++) {
							if (scriptArray[n].length() != 0) {
								writer.write(scenarios.replace("scenarioOne",
										scenarioArray[n]) + "\n");
								writer.write(scriptTemplate.replace(
										"scriptName", scriptArray[n]) + "\n");
							}
						}
					}
				}

				UpdateJmx updateJmx = new UpdateJmx(script1, script2, script3,
						script4, script5, scriptPath, slaveWorkSpacePath,
						getterSetter, scriptIndex);
				updateJmx.updateScript_Configuration_Variable_Path();

			}
			writer.close();
			if (!System.getProperty("os.name").toLowerCase()
					.contains("windows")) {
				Set<PosixFilePermission> perms = new HashSet<>();
				perms.add(PosixFilePermission.OWNER_READ);
				perms.add(PosixFilePermission.OWNER_WRITE);
				perms.add(PosixFilePermission.OWNER_EXECUTE);
				perms.add(PosixFilePermission.GROUP_READ);
				perms.add(PosixFilePermission.GROUP_WRITE);
				perms.add(PosixFilePermission.GROUP_EXECUTE);
				perms.add(PosixFilePermission.OTHERS_READ);
				perms.add(PosixFilePermission.OTHERS_WRITE);
				perms.add(PosixFilePermission.OTHERS_EXECUTE);
				Files.setPosixFilePermissions(yamlFilname.toPath(), perms);
			}
		} catch (FileNotFoundException e) {
			System.out
					.println("FileNotFoundException in createYamlConfig method:"
							+ e);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("IOException in createYamlConfig method:" + e);
		} catch (Exception e) {
			resultPath = null;
			e.printStackTrace();
			System.out.println("Exception in createYamlConfig method:" + e);
		} finally {
			if (writer != null) {
				writer.close();
			}
		}
		return resultPath;
	}
}
