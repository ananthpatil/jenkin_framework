import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class UpdateJmx {
	private String script1, script2, script3, script4, script5;
	private String scriptParentPath, slaveWorkSpacePath;
	private GetterAndSetter getterSetter;
	private int scriptIndex;

	/**
	 * <p>
	 * Constructor to initialize the script varaible and Its parents path
	 * </p>
	 * 
	 * @param script1
	 * @param script2
	 * @param script3
	 * @param script4
	 * @param script5
	 * @param scriptParentPath
	 */
	UpdateJmx(String script1, String script2, String script3, String script4,
			String script5, String scriptParentPath, String slaveWorkSpacePath,
			GetterAndSetter getterSetter,int scriptIndex) {
		this.script1 = script1;
		this.script2 = script2;
		this.script3 = script3;
		this.script4 = script4;
		this.script5 = script5;
		this.scriptParentPath = scriptParentPath;
		this.slaveWorkSpacePath = slaveWorkSpacePath;
		this.getterSetter = getterSetter;
		this.scriptIndex = scriptIndex;
		// this.rsltFolderPath = rsltFolderPath;
	}

	/**
	 * <p>
	 * The method used to update the scripts configuration variables path
	 * </p>
	 */
	public void updateScript_Configuration_Variable_Path() {
		String[] scriptArray = { script1, script2, script3, script4, script5 };
		String modeOfExecution = getterSetter.getExecutionMode().toLowerCase();
		for (int s = 0; s < scriptArray.length; s++) {
			if (scriptArray[s].length() != 0) {
				File jmxFile = new File(scriptParentPath + scriptArray[s]);
				DocumentBuilderFactory dFactory;
				DocumentBuilder dBuilder;
				if (jmxFile.exists()) {
					try {
						dFactory = DocumentBuilderFactory.newInstance();
						dBuilder = dFactory.newDocumentBuilder();
						Document doc = dBuilder.parse(jmxFile);

						doc.getDocumentElement().normalize();
						System.out.println("Root element :"
								+ doc.getDocumentElement().getNodeName());

						String parentfolder = jmxFile.getParentFile()
								.getParent();
						File tstDataFolder = new File(parentfolder
								+ File.separator + "TestData");
						String testDataFolderPath = tstDataFolder
								.getAbsolutePath() + File.separator;
		
						if (modeOfExecution.equals("distributed") && s != (scriptIndex-1)) {
							testDataFolderPath = testDataFolderPath
									.substring(testDataFolderPath.lastIndexOf("workspace"));
							testDataFolderPath = slaveWorkSpacePath
									+ File.separator + testDataFolderPath;
						}

						/**
						 * The following code to set configuration in user
						 * defined variables section for result and report
						 * folder path
						 */
						try {
							NodeList userDefinedVar = doc
									.getElementsByTagName("Arguments");
							String enabled, attrName;

							for (int u = 0; u < userDefinedVar.getLength(); u++) {
								Element usrElements = (Element) userDefinedVar
										.item(u);
								enabled = usrElements.getAttribute("enabled");
								if (enabled.toLowerCase().equals("true")) {
									NodeList userChildEles = usrElements
											.getElementsByTagName("*");
									for (int u1 = 0; u1 < userChildEles
											.getLength(); u1++) {
										Element ele = (Element) userChildEles.item(u1);
										attrName = ele.getAttribute("name").trim();
										NodeList nlist = ele.getChildNodes();
									   if(attrName.equals("rsltFldPath")){
										   String txtValue = nlist.item(3).getTextContent();
										if(txtValue.contains("setProperty") && txtValue.contains("rsltFldPath")){
											if(modeOfExecution.equals("distributed"))
												txtValue = txtValue.replace("-G", "").replace("-J", "")
												       .replace("rsltFldPath", "-GrsltFldPath");
							
										    else
										    	txtValue = txtValue.replace("-G", "").replace("-J", "")
										    	       .replace("rsltFldPath", "-JrsltFldPath");
											nlist.item(3).setTextContent(txtValue);
										 }
									   }
										if (attrName.equalsIgnoreCase("tstDataFldPath") || attrName
														.equalsIgnoreCase("tstFldPath")) {
											nlist.item(3).setTextContent(testDataFolderPath);
										}
									}
									break;
								}
							}
						} catch (Exception e) {
							System.out
									.println("The execption occured while updating the "
											+ "rsltFldPath variable" + e);
						}
						TransformerFactory transformerFactory = TransformerFactory
								.newInstance();
						Transformer transformer = transformerFactory
								.newTransformer();
						DOMSource source = new DOMSource(doc);
						StreamResult result = new StreamResult(scriptParentPath
								+ scriptArray[s]);
						transformer.transform(source, result);
						System.out
								.println("====The succesfully updated the jmx file===");
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					System.out.println("The file doesnt exist:" + jmxFile);
				}
			}
		}
	}
}
