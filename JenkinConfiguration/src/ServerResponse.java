
import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ServerResponse
 */
// @WebServlet("/ServerResponse")
public class ServerResponse extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServerResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");

		List<String> consoleOutput = (List<String>) request
				.getAttribute("consoleOutput");
		request.setAttribute("consoleOutput", consoleOutput);

		String buildStatus = (String) request.getAttribute("buildStatus");
		request.setAttribute("buildStatus", buildStatus);

		RequestDispatcher dispatch = request
				.getRequestDispatcher("serverresponse1.jsp");
		dispatch.forward(request, response);
	}

}
