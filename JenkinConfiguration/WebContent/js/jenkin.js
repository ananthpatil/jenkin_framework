/** The method to validate the 
 *   clear the entirfe form
 */
function clearForm(){
     document.getElementById('myForm').reset();
}

function invalidInput(element,func){
		element.setCustomValidity('Please enter valid IP format ex:10.174.16.193');
		func(element);
}
function borderSet(element){
	element.style.border='1px solid red';
}

/** The method to validate the 
 *    whether inputed value is IP or not
 */
function onBlur(ipAddress){
	    val = ipAddress.value;
		//debugger
		attrName = ipAddress.getAttribute('name');
		if(val.match("\\d+\\.\\d+\\.\\d+\\.\\d+")){
		    ipAddress.setCustomValidity('');
		    ipAddress.style.border='1px solid #d7d7d7';	
		}else{
			if(attrName==='node1'){
				ipAddress.style.border='1px solid red';
			    ipAddress.setCustomValidity('Please enter valid IP format ex:10.174.16.193');
			 }else{
			   if(val.length==0){
				    ipAddress.setCustomValidity('');
		            ipAddress.style.border='1px solid #d7d7d7';
			   }else{
				   ipAddress.style.border='1px solid red';
			       ipAddress.setCustomValidity('Please enter valid IP format ex:10.174.16.193');  
			   }
			}
		}
}

/** The method to validate the 
 *   whether inputed value is in 
 *   in proper memory format or not
 */
function memoryFormatValidation(element){
    val = element.value;
    if(val.match("^[0-9]+[\\sa-zA-Z]+$")){
        element.setCustomValidity('');
		element.style.border='1px solid #d7d7d7';	
    }else{
		element.style.border='1px solid red';
        element.setCustomValidity('Please enter the memory size Ex:4 GB');
	}	
}
	
/** The method to validate the 
 *    whether inputed value is path or not
 */
function pathValidation(element){
	jmeterPath = element.value;
	if(jmeterPath.match("(^([\\w]+\\:|\\\\)[\\\\_\\s\\-\\w0-9.]+)|(^([\\w]+\\:|/)[/_\\s\\-\\w0-9.]+)")){
		element.style.border='1px solid #d7d7d7';
		element.setCustomValidity('');
		//outline: none;
	}else{
		element.setCustomValidity('Please enter the proper path:');
		element.style.border='1px solid red';
	}
}

/** The method to validate the 
 *    whether file name is Yaml or not
 */
function yamlValidation(element){
	yamlName = element.value;
	if(yamlName.match("^[a-zA-Z]+[a-zA-Z0-9\\s\\-_]+\\.(ya*ml)$")){
		element.setCustomValidity('');
		element.style.border='1px solid #d7d7d7';
	}else{
		element.setCustomValidity('Please enter the yaml file name:');
		element.style.border='1px solid red';
	}
}

function pageLoader(){
 try {	 
	 document.body.classList.add("bodyclass");
	 /*emptyLoaderParent = document.getElementsByClassName('empty');
	 emptyLoaderParent[0].classList.add('emptyload');*/
	 loaderEle = document.getElementsByClassName('emptyloader');
     loaderEle[0].classList.add("loader");
	 loaderEle[0].style.display='block';
    }
  catch(err) {
    console.log(err.message);
  }  
}

/** The method to validate the 
 *    form data
 */
function captureFormData(){
	  var nodes = document.getElementsByClassName("node");
	  var scripts = document.getElementsByClassName("scriptone");
	  var drpdownValue = document.getElementById('sel1').value;
	  var flag = true;
	  
	  loop1:
	  for(var j=0;j<nodes.length;j++){
		  var node,script;
		  node = nodes[j].value;
		  script = scripts[j].value;
		  
		  if(node.length != 0 && script.length == 0){
			  window.alert('Please specify the script name');
			  flag = false;
			  break loop1;
		  }
		  if(node.length == 0 && script.length != 0){
			  //nodes[j].style.border="1px solid red";
			  window.alert('Please specify Machine IP');
			  flag = false;
			  break loop1;
		  }
		if(drpdownValue.toLowerCase().includes('distributed')){
		  for(var i=j+1;i<nodes.length;i++){
		        var nextNode = nodes[i].value;
		        if(node === nextNode && node.length != 0){
			     window.alert('Node'+(j+1)+' IP value duplicated');
				 flag = false;
				 break loop1;
			   }
		   }
		}
	  }
	 if(flag===true){
	    pageLoader();
	 }
	  return flag;
}

/** The method to validate the 
 *    whether file name is jmx or not
 */
function scriptValidation(element){
	    var val = element.value;
		//debugger;
	   if(val.includes('.jmx')){
			 element.setCustomValidity('');
			 element.style.border='1px solid #d7d7d7';
       }
	   else
	   if(val.length == 0){
			 element.setCustomValidity('');
			 element.style.border='1px solid #d7d7d7';
       }
	   else{
			 element.style.border='1px solid red';
			 element.setCustomValidity('Script doesnt seems jmx file');
	   }
}
/**  The method to validate the
 *   dropdown selected value
 */
function dropdownValidation(element){
     var value =  element.value;
	 if(value.toLowerCase().includes('distributed')){
	   element.style.border='1px solid #d7d7d7';
	   element.setCustomValidity('');
    }
	else
	if(value.toLowerCase().includes('standalone')){
	   element.style.border='1px solid #d7d7d7';
	   element.setCustomValidity('');
	}else{
			 element.style.border='1px solid red';
			 element.setCustomValidity('Please select one value');
	}
}
