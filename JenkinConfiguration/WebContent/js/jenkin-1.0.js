/** The method to validate the 
 *   clear the entirfe form
 */
function clearForm(){
     document.getElementById('myForm').reset();
}

function invalidInput(element,func){
		element.setCustomValidity('Please enter valid IP format ex:10.174.16.193');
		func(element);
}
function borderSet(element){
	element.style.border="1px solid red";
}

/** The method to validate the 
 *    whether inputed value is IP or not
 */
function onBlur(ipAddress){
	    val = ipAddress.value;
		//debugger
	    console.log("entere here"+val);
		if(val.match("\\d+\\.\\d+\\.\\d+\\.\\d+")){
		    ipAddress.setCustomValidity('');
		    ipAddress.style.border="1px solid #b9b3b3b3";	
		}else{
			ipAddress.style.border="1px solid red";
			ipAddress.setCustomValidity('Please enter valid IP format ex:10.174.16.193');
		}
}

/** The method to validate the 
 *   whether inputed value is in 
 *   in proper memory format or not
 */
function memoryFormatValidation(element){
    val = element.value;
    if(val.match("^[0-9]+[\\sa-zA-Z]+$")){
        element.setCustomValidity('');
		element.style.border="1px solid #b9b3b3b3";	
    }else{
        element.setCustomValidity('Please enter the memory size Ex:4 GB');
	}	
}
	
/** The method to validate the 
 *    whether inputed value is path or not
 */
function pathValidation(element){
	jmeterPath = element.value;
	console.log(jmeterPath);
	if(jmeterPath.match("(^([\\w]+\\:|\\\\)[\\\\_\\s\\-\\w0-9.]+)|(^([\\w]+\\:|/)[/_\\s\\-\\w0-9.]+)")){
		element.style.border="1px solid #b9b3b3b3";
		element.setCustomValidity('');
		//outline: none;
	}else{
		element.setCustomValidity('Please enter the proper path:');
		element.style.border="1px solid red";
	}
}

/** The method to validate the 
 *    whether file name is Yaml or not
 */
function yamlValidation(element){
	yamlName = element.value;
	if(yamlName.match("^[a-zA-Z]+[a-zA-Z0-9\\s\\-_]+(.yml|.yaml)$")){
		element.setCustomValidity('');
		element.style.border="1px solid #b9b3b3b3";
	}else{
		element.setCustomValidity('Please enter the yaml file name:');
		element.style.border="1px solid red";
	}
}

/** The method to validate the 
 *    form data
 */
function captureFormData(){
	  var nodes = document.getElementsByClassName("node");
	  var scripts = document.getElementsByClassName("scriptone");
	  
	  var flag = true;
	  for(var j=0;j<nodes.length;j++){
		  var node,script;
		  node = nodes[j].value;
		  script = scripts[j].value;
		  
		  if(node.length= 0 && script.length == 0){
			  window.alert('Please specify the script name');
			  flag = false;
			  break;
		  }
		  if(node.length == 0 && script.length != 0){
			  //nodes[j].style.border="1px solid red";
			  window.alert('Please specify Machine IP');
			  flag = false;
			  break;
		  }
	  }
	  return flag;
}

/** The method to validate the 
 *    whether file name is jmx or not
 */
function scriptValidation(element){
	    var val = element.value;
		//debugger;
	   if(val.includes('.jmx')){
			 element.setCustomValidity('');
			 element.style.border="1px solid #b9b3b3b3";
       }else{
			 element.style.border='1px solid red';
			 element.setCustomValidity('Script doesnt seems jmx file');
	   }
}
