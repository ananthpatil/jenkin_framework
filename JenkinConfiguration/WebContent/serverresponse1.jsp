<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Server Log</title>
<style>
.header {
	width: 100%;
	min-height: 50px;
	padding-top: 8px;
	padding-bottom: 6px;
	background: #70cbf4;
	color: #ffffff;
	position: fixed;
	background-repeat: no-repeat;
	top: 0px;
	z-index: 1;
}

.header img {
    width: 100%;
    max-width: 130px;
    position: relative; 
    margin-top: -86px
 }

body {
	font-family: Tahoma, Geneva, sans-serif;
	background-color: #dbe5f3;
	/*background-color: yellow;*/
}

.container {
	max-width: 1024px;
	margin: 105px auto;
	padding-top: 27px;
	border-left: 1px solid #dcdada;
	border-right: 1px solid #dcdada;
	padding-left: 60px;
	padding-right: 16px;
	min-height: 600px;
	background-color: #ffffff;
    /* border: 1px dotted black;
    overflow: scroll; */
}
.inner_container{
	overflow:auto;
	height: 100%;
}
h1 {
	/*margin: 0;*/
}

.content{

}
</style>
</head>
<body>
	<div class="header">
		<h1 align="center">Server Response</h1>
       <img src="./images/logo.png"/>
	</div>
	<div class="container">
	<div class="inner_container">
	<c:forEach items ="${consoleOutput}"  var="console" >
			<div class="content">
				<p> ${console} </p>
			</div>

   </c:forEach>
   <div class="content">
				<p> ${buildStatus} </p>
	</div>
   
	</div>
		
	</div>
</body>
</html>